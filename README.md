ONE WAY TO DIE: DESKTOP EDITION | COAGUCO INDUSTRIES

"What is this?!"

The desktop edition of the One Way To Die text-misadventure game from CoaguCo Industries.  Or an exercise in futility.  Play the original browser version at http://die.coaguco.com

"A synopsis?"

Try to make it from your house to the Finnigan Brothers' Fun-Park for its last day open, then return without dying.  There are three things to aid you in your quest: aspirin, a sandwich, and gasoline.  Good luck.

"How do I get set up?"

There are three options: use an installer for Linux, Windows, or Mac.  Download it from Desura.  Or install everything from source.

"I will do a source installation!"

The game requires Python 2.7 or greater, Pygame 1.9.1 or greater, and Python module Requests.  All of these files are available in the dependencies folder in Linux, Windows, and Mac flavors.

And, of course, you will the need the game itself.  It is available at the official repository at https://bitbucket.org/coagucoindustries/one-way-to-die-desktop-edition-public

"Can I tinker with this?"

The code is all in Python so the scripts can be editied.  The game itself can be modified or rebuilt into something else.  It can even be redistributed in full, including torrenting!  However, we ask you do not use the assets for anything else and include our copyright if you distribute it.  Oh, and don't charge money for it!

Copyright 2014 CoaguCo Industries | http://coaguco.com
