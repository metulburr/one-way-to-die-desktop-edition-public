################################################
# CoaguCo Industries | GP Garcia
# One Way To Die Desktop Edition
################################################
import pygame, os, sys
sys.path.append("src")
import boot, process, splash, menu, game, player, options, leaders, stats, credits

def main():
	# Run the boot process
	boot
	# Load the player class
	pc = player.Player()
	# If the player save is loaded, populate player statistics
	if boot.LOGGED and boot.NETWORK:
		process.retrieveStats(pc)
	# Run the main program loop
	while boot.PLAY_GAME:
		# Switch game states
		if boot.STATE == "splash":
			splash.Splash()
		elif boot.STATE == "menu":
			menu.Menu()
		elif boot.STATE == "options":
			options.Options(pc)
		elif boot.STATE == "game":
			game.Game(pc)
		elif boot.STATE == "leaders":
			leaders.Leaders()
		elif boot.STATE == "stats":
			stats.Stats(pc)
		elif boot.STATE == "credits":
			credits.Credits()
		elif boot.STATE == 'quit':
			pygame.quit()
			os._exit(1)

if __name__ == "__main__":
	main()
